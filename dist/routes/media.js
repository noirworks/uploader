"use strict";

var _express = _interopRequireDefault(require("express"));

var _db = _interopRequireWildcard(require("../db"));

var _multer = _interopRequireDefault(require("multer"));

var _config = _interopRequireDefault(require("../services/uploader/config"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//(async () => {
// if(db){
//     await db.sync({ force: true });
// }
//})();
_db.default.sync();

const uploadConfig = {
  allowMimeTypes: ['image/jpeg']
};

const storage = _multer.default.diskStorage({
  destination: async (req, file, cb) => {
    const dir = req.dest;
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    const [name, ext] = file.originalname.split('.');
    const newFileName = `${name}-${Date.now()}.${ext}`;
    cb(null, newFileName);
  }
});

const upload = (0, _multer.default)({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 // = 1MB

  },
  dest: `${process.env.PWD}/public/uploads2/`,
  fileFilter: (req, file, cb) => {
    if (!uploadConfig.allowMimeTypes.includes[file.mimetype]) {
      cb(null, true);
    }

    ;
    cb(null, true);
  }
});

const router = _express.default.Router();

const fileModel = file => {
  return {
    id: file.id,
    filename: file.filename,
    bucket: file.bucket,
    title: file.title,
    mimetype: file.mimetype,
    external_id: file.external_id,
    private: file.private,
    fullpath: getFilePath(file) //fullPath

  };
};

const fileModels = files => {
  return files.map(file => {
    return fileModel(file);
  });
};

const saveToDb = async files => {
  const res = await _db.default.models.file.bulkCreate(files).then(r => {
    console.log("RRRR", r);
    return r;
  }).catch(e => {
    console.log("EEEEEE", e);
  });
  return res;
};

const getFilePath = (file, size = 'original') => {
  return `${file.private ? _config.default.privatePathUploads : _config.default.publicPathUploads}/${file.app}/${size}/${file.filename}`;
};

router.get('/', async (req, res, next) => {
  const items = await _db.default.models.file.findAll();
  const models = fileModels(items);
  res.json({
    'files': models,
    'ok': items.length ? true : false
  });
});
router.get('/:id', async (req, res, next) => {
  const id = req.params.id;
  const item = await _db.default.models.file.findOne(id);
  const model = item ? fileModel(item) : null;
  res.json({
    'file': model,
    'ok': item ? true : false
  });
});
router.post('/', upload.any(), async (req, res, next) => {
  let files = req.files.map(f => {
    return {
      filename: f.filename,
      bucket: req.authorizedApp.user,
      //f.fieldname,
      app: req.authorizedApp.key,
      title: f.originalname,
      mimetype: f.mimetype,
      external_id: '',
      private: false
    };
  });
  let items = files && files.length ? await saveToDb(files) : [];
  const models = items && items.length ? items.map(item => {
    return { ...fileModel(item)
    };
  }) : [];
  res.json({
    "ok": items.length ? true : false,
    "files": models,
    "length": models.length
  });
});
router.post('/:id', async (req, res, next) => {
  const id = req.params.id;
  const item = await _db.default.models.file.findOne(id); // update here

  const model = fileModel(item);
  res.json({
    'file': model,
    'ok': item ? true : false
  });
});
router.delete('/:id', async (req, res, next) => {
  const id = req.params.id;
  const item = await _db.default.models.file.findOne(id);
  let status = false;

  if (item) {
    item.destroy();
    status = true;
  }

  res.json({
    'ok': status,
    'file': status ? {
      id
    } : false
  });
});
module.exports = router;