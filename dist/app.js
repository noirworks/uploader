"use strict";

var _middleware = require("./services/uploader/middleware");

var _auth = require("./auth");

var createError = require('http-errors');

var express = require('express');

var path = require('path');

var cookieParser = require('cookie-parser');

var logger = require('morgan');

require('./env');

var indexRouter = require('./routes/index');

var mediaRouter = require('./routes/media');

var privateRouter = require('./routes/private');

var uploadsRouter = require('./routes/uploads');

var app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/uploads', uploadsRouter);
app.use('/media', _auth.middleware, _middleware.appMiddleware, mediaRouter);
app.use('/private', _auth.middleware, privateRouter); // catch 404 and forward to error handler

app.use(function (req, res, next) {
  next(createError(404));
}); // error handler

app.use(function (err, req, res, next) {
  console.warn("err.message", err.message); // set locals, only providing error in development

  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {}; // render the error page

  res.status(err.status || 500);
  res.json({
    'err.message': err.message
  });
});
module.exports = app;