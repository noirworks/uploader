"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = require("sequelize");

const config = process.env;
const modules = [{
  moduleName: 'file',
  model: (sequelize, DataTypes) => {
    const File = sequelize.define('file', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      filename: {
        type: DataTypes.STRING,
        allowNull: true
      },
      bucket: {
        type: DataTypes.STRING,
        allowNull: true
      },
      app: {
        type: DataTypes.STRING,
        allowNull: true
      },
      title: {
        type: DataTypes.STRING,
        allowNull: true
      },
      mimetype: {
        type: DataTypes.STRING,
        allowNull: true
      },
      private: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false
      }
    });

    File.findOne = async id => {
      const items = await File.findAll({
        where: {
          id: id
        }
      });
      return items.length ? items[0] : null;
    };

    return File;
  }
}];
const sequelize = new _sequelize.Sequelize(config['DB_NAME'], config['DB_USER'], config['DB_PASSWORD'], {
  host: config['DB_HOST'] || 'localhost',
  port: config['DB_PORT'],
  dialect: config['DB_DIALECT'] || 'mysql',
  debug: false
});
modules.forEach(mod => {
  if (mod.moduleName && mod.model) {
    sequelize.import(mod.moduleName, mod.model);
  }
});
sequelize.sync();
var _default = sequelize;
exports.default = _default;