const dotenv = require('dotenv');
const fs = require('fs');

const files = [
    process.env['NODE_ENV'] === 'production' ? '.env.production.local' : null,
    process.env['NODE_ENV'] === 'development' ? '.env.development.local' : null,
    '.env.local',
    process.env['NODE_ENV'] === 'production' ? '.env.production' : null,
    process.env['NODE_ENV'] === 'development' ? '.env.development' : null,
    '.env'
];

files
.filter(d=>d)
.forEach(file => {
    if( file ){
        const filePath = `${process.env.PWD}/env/${file}`;
        if( fs.existsSync(filePath) ){
            dotenv.config({ path: filePath });
        }
    }
})
