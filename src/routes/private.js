var express = require('express');
var router = express.Router();
import path from 'path';
import config from '../services/uploader/config';

/* GET home page. */
/*router.get('/*', function(req, res, next) {
    console.log("PRIV", req);
    res.json('priv');
});*/

/*router.get('/*', function(req, res, next) {
    console.log("PRIV", path.join(__dirname, '/../../private'));
    res.json('priv');
});*/
//path.join(__dirname, '/../../private')
router.get('/*', express.static( config.privateUploads ));

module.exports = router;
