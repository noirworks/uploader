var express = require('express');
var router = express.Router();
import path from 'path';
import config from '../services/uploader/config';

//path.join(__dirname, '/../../private')
router.get('/*', express.static( config.publicUploads ));
//router.get('/*', express.static(path.join(__dirname, '/../../public/uploads')));

module.exports = router;
