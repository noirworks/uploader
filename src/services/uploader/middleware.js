import {getAppUploadDirParts, getAppUploadDir, dirPartsExistOrCreate} from './tools';

const applyDestination = async (req, res, next) => {
    return req.dest = await getAppUploadDir(req.authorizedApp);
};

const makeUploadsDir = async (req) => {
    console.log("middleware makeUploadsDir start");
    const parts = await getAppUploadDirParts(req.authorizedApp);
    await dirPartsExistOrCreate(parts);
    console.log("middleware makeUploadsDir end");
};

export const uploadMiddleware = async (req, res, next) => {
    next();
};

export const appMiddleware = async (req, res, next) => {
    try {
        await applyDestination(req, res, next);
        await makeUploadsDir(req);
        next();
    } catch (error) {
        next(error);
    }
};
