import fs from 'fs';
import config from './config';
import path from 'path';

export const existsAsync = async (path) => {
    return new Promise(function(resolve, reject){
      fs.exists(path, function(exists){
        resolve(exists);
      })
    })
}

export const mkdirAsync = async (path) => {
    return new Promise(function(resolve, reject){
        fs.mkdir(path, function(err){
            if(err){
                reject(err);
            }else{
                resolve();
            }
        })
    })
}

export const dirExist = async (dir) => {
    return await existsAsync(dir);
};

export const makeDir = async (dir) => {
    const res = await mkdirAsync(dir);
    console.log("makeDir, ", dir, res);
    return true;
    /*const res = await fs.mkdir(dir, async (err) => {
        if(err) {
            console.log("ERROR ", dir);
            console.warn('Error in folder creation', err);
            return false;
        }else{
            console.log("ERROR ", dir);
            return true;
        }
    });
    console.log("makeDir, ", dir, res);
    return true;*/
};

export const dirExistOrCreate = async (dir) => {
    await mkdirs(dir);
    return true;
};

function mkdirs(dirname){
    if(fs.existsSync(dirname)){
      return true;
    }else{
      if(mkdirs(path.dirname(dirname))){
        fs.mkdirSync(dirname);
        return true;
      }
    }
}

export const dirPartsExistOrCreate = async (parts) => {
    console.log(parts);
    await mkdirs( parts.join('/') );
    /*await parts.map(async (part,i) => {
        const dir = parts.slice(0,i+1).join('/');
        console.log("APP MIDLLEWARER dir", dir);
        return await dirExistOrCreate( dir );
    });*/
    return true;
};

export const getAppUploadDirParts = (app, size = 'original') => {
    return [
        //config.basePath,
        app.private ? config['privateUploads'] : config['publicUploads'],
        //'uploads',
        app.key,
       // app.user || 'general',
        size
    ].filter(a=>a);
};

export const getAppUploadDir = async (app) => {
    const parts = getAppUploadDirParts(app);
    return parts.join('/');
};
