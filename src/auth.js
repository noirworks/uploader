const apps = [
    {
        key: '1st',
        title: 'First',
        auth: '1st1st'
    }
];

const getAppHeadersFromReq = (req) => {
    return {
        key: req.headers['contiv-app'] || null,
        auth: req.headers['contiv-auth'] || null,
        user: req.headers['contiv-user'] || null,
        isPrivate: req.headers['contiv-private'] || null
    };
};

export const middleware = (req, res, next) => {
    const { key, auth, user, isPrivate } = getAppHeadersFromReq(req);
    if(!key || !auth){ throw new Error('Unauthorize. Empty request variables.'); };
    const app = apps.find(a=>a.key === key);
    if(!app){ throw new Error('Unauthorize. Wrong app.'); };
    if(app.auth !== auth){ throw new Error('Unauthorize app.'); };
    req.authorizedApp = { ...app, private: isPrivate, user };
    next();
};




/*

//bad
[ 1, 2, 3, 4, 5 ].filter(num => (num > 3))

//good
const checkNum = num => (num > 3);
[ 1, 2, 3, 4, 5 ].filter(checkNum)

*/
